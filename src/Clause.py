from enum import Enum
import typing as tp

from utils import (
    Decision,
)


class ClauseType(Enum):
    UNSAT: int = 0
    UNIT: int = 1
    UNRESOLVED: int = 2
    SAT: int = 3


class Clause:
    def __init__(self, formula: tp.List[int]):
        self._formula = {
            abs(var): (var > 0)
            for var in formula
        }


    def items(self):
        return self._formula.items()

    def is_variable_in(self, variable_number):
        return variable_number in self._formula

    def get_variable_value(self, variable_number):
        return self._formula[variable_number]

    def check(
            self,
            current_decisions: tp.List[Decision],
            current_level: int,
            antecedent: int,
    ) -> tp.Tuple[int, tp.Optional[Decision]]:

        var_values = {}
        for decision in current_decisions:
            var = decision.variable_number
            value = decision.variable_value

            var_value = var_values.get(var)

            if var_value is not None and var_value != value:
                #  Made two different decisions for one variable
                return ClauseType.UNSAT, None
            var_values[var] = value

        used = {
            var: False
            for var in self._formula
        }
        false_count = 0
        for var, needed_value in self._formula.items():
            if var in var_values:
                var_res = (var_values[var] == needed_value)
                if var_res:
                    return ClauseType.SAT, None
                else:
                    used[var] = True
                    false_count += 1

        cnt_unused = sum(1 for var, use_stat in used.items() if not use_stat)
        if cnt_unused == 1:  # Unit clause
            unused_var = [var for var, use in used.items() if not use][0]
            return ClauseType.UNIT, Decision(
                variable_number=unused_var,
                variable_value=self._formula[unused_var],
                decision_level=current_level,
                antecedent=antecedent,
            )
        elif false_count == len(self._formula):  # False clause
            return ClauseType.UNSAT, None
        else:
            return ClauseType.UNRESOLVED, None
