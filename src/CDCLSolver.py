from enum import Enum

import logging
import os
from pathlib import Path
import random
import sys
import typing as tp

from utils import (
    CDCLTask, CDCLTaskDIMACS, Decision
)
import Clause


class CDCLStatus(Enum):
    RUNNING: int = 0
    SAT: int = 1
    UNSAT: int = 2
    STOP: int = 3


class CDCLSolver:
    def __init__(self, decision_mode: str = 'random'):
        self.logger = logging.Logger(name='CDCL logger')
        self._status = CDCLStatus.RUNNING
        self._decision_mode = decision_mode
        self._decision_functions = {
            'random': self._make_random_decision,
        }

    def _init_by_task(self, task: CDCLTask):
        self._num_variables = task.num_variables
        self._clauses = []
        for task_clause in task.clauses:
            clause = Clause.Clause(task_clause)
            self._clauses.append(clause)
        self._decision_level = 0
        self._decisions = []
        self._previous_decisions = set()

    def _make_decision(self, **kwargs):
        return self._decision_functions[self._decision_mode](**kwargs)

    def _make_random_decision(
            self,
            current_decisions: tp.List[Decision],
            current_level: int
    ) -> tp.Optional[Decision]:
        used_vars = {decision.variable_number for decision in
                     current_decisions}
        unused_vars = list(
            {i for i in range(1, self._num_variables + 1)} - used_vars)

        all_possible_decisions = set()
        for var in unused_vars:
            any_possible_val = False
            for value in [False, True]:
                if (var, value) not in self._previous_decisions:
                    all_possible_decisions.add((var, value))
                    any_possible_val = True
            if not any_possible_val:
                # print(f"None values for variable {var}")
                return None

        all_possible_decisions -= self._previous_decisions

        decision_var, var_value = random.choice(list(all_possible_decisions))
        self._previous_decisions.add((decision_var, var_value))
        # decision_var = random.choice(unused_vars)
        # var_value = random.choice([False, True])
        # self.logger.info(
        # print(f"Make random decision variable {decision_var} assign to value {var_value}")

        return Decision(decision_var, var_value, current_level, -1)

    def _unit_propagation(self) -> bool:
        # Return false if conflict
        formula = self._clauses
        decisions = self._decisions

        unit_clause_found = True
        while unit_clause_found:
            unit_clause_found = False
            for clause_ind, clause in enumerate(formula):
                clause_stat, possible_decision = clause.check(
                    current_decisions=decisions,
                    current_level=self._decision_level,
                    antecedent=clause_ind
                )
                if clause_stat in (
                        Clause.ClauseType.SAT, Clause.ClauseType.UNRESOLVED):
                    self._kappa_antecedent = -1
                    continue
                if clause_stat == Clause.ClauseType.UNIT:
                    # print(f"Make UNIT clause decision {possible_decision.variable_number} assign to value {possible_decision.variable_value}")
                    self._decisions.append(possible_decision)
                    unit_clause_found = True
                if clause_stat == Clause.ClauseType.UNSAT:
                    self._kappa_antecedent = clause_ind
                    return False
        return True

    def _resolve(self, learnt_clause: Clause.Clause, resolver_var: int):
        resolver_decision_level, resolver_antecedent \
            = self._get_literal_decision_level_antecedent(resolver_var)
        second_clause = self._clauses[resolver_antecedent]
        res_clause = []
        for variable_number in range(self._num_variables):
            if learnt_clause.is_variable_in(
                    variable_number) and second_clause.is_variable_in(
                variable_number):
                if learnt_clause.get_variable_value(
                        variable_number) == second_clause.get_variable_value(
                    variable_number):
                    if learnt_clause.get_variable_value(variable_number):
                        res_clause.append(variable_number)
                    else:
                        res_clause.append(-variable_number)
            elif learnt_clause.is_variable_in(variable_number):
                if learnt_clause.get_variable_value(variable_number):
                    res_clause.append(variable_number)
                else:
                    res_clause.append(-variable_number)
            elif second_clause.is_variable_in(variable_number):
                if second_clause.get_variable_value(variable_number):
                    res_clause.append(variable_number)
                else:
                    res_clause.append(-variable_number)
        return Clause.Clause(res_clause)

    def _get_literal_decision_level_antecedent(self, literal: int):
        literal_decision_level = -1
        literal_antecedent = -1
        for decision in self._decisions:
            if decision.variable_number == literal:
                literal_decision_level = decision.decision_level
                literal_antecedent = decision.antecedent
                break
        return literal_decision_level, literal_antecedent

    def _conflict_analysis(self) -> int:
        learnt_clause = self._clauses[self._kappa_antecedent]
        conflict_decision_level = self._decision_level
        this_level_count = 0
        while True:
            this_level_count = 0
            resolver_var = -1
            for var, _ in learnt_clause.items():
                var_decision_level, var_antecedent \
                    = self._get_literal_decision_level_antecedent(var)
                if var_decision_level == conflict_decision_level:
                    this_level_count += 1
                if var_decision_level == conflict_decision_level and var_antecedent != -1:
                    resolver_var = var
            if this_level_count == 1:
                break
            learnt_clause = self._resolve(learnt_clause, resolver_var)
        self._clauses.append(learnt_clause)

        backtracked_decision_level = 0
        for var, _ in learnt_clause.items():
            var_decision_level = -1
            var_antecedent = -1
            var_decision_level, var_antecedent \
                = self._get_literal_decision_level_antecedent(var)
            if var_decision_level > backtracked_decision_level \
                    and var_decision_level != conflict_decision_level:
                backtracked_decision_level = var_decision_level

        decision_to_leave = []
        for decision in self._decisions:
            if decision.decision_level <= backtracked_decision_level:
                decision_to_leave.append(decision)
        self._decisions = decision_to_leave
        return backtracked_decision_level

    def solve(self, task: CDCLTask) -> (int, tp.List[Decision]):
        self._init_by_task(task)

        while len(self._decisions) != self._num_variables:
            self._decision_level += 1
            decision = self._make_decision(
                current_decisions=self._decisions,
                current_level=self._decision_level,
            )
            if decision is None:
                # print("DECISION IS NONE LUL")
                return CDCLStatus.UNSAT, None
            self._decisions.append(decision)
            if not self._unit_propagation():
                if self._decision_level == 0:
                    return CDCLStatus.UNSAT, None
                backtrack_decision_level = self._conflict_analysis()
                if backtrack_decision_level < 0:
                    # print("BACKTRACK LUL")
                    return CDCLStatus.UNSAT
                else:
                    # print("back")
                    self._decision_level = backtrack_decision_level
            else:
                pass

        return CDCLStatus.SAT, self._decisions


class CDCLReader:
    def __init__(self):
        pass

    def read_task_dimacs(self, path: os.PathLike):
        with open(path, 'r') as inp:
            pref, comment = inp.readline().split(maxsplit=1)
            _, mode, num_variables, num_clauses = inp.readline().split()
            num_variables, num_clauses = int(num_variables), int(num_clauses)
            clauses = [
                list(map(int, inp.readline().split()))[:-1]
                for i in range(num_clauses)
            ]
        return CDCLTaskDIMACS(
            comment=comment,
            num_clauses=num_clauses,
            num_variables=num_variables,
            clauses=clauses
        )


if __name__ == "__main__":
    input_path = Path(sys.argv[1])

    reader = CDCLReader()
    task = reader.read_task_dimacs(path=input_path)

    solver = CDCLSolver()
    result = solver.solve(task)
    if result[0] == CDCLStatus.SAT:
        print("SAT")
        for decision in result[1]:
            if decision.variable_value:
                print(decision.variable_number, end=" ")
            else:
                print(-decision.variable_number, end=" ")
        print()
    else:
        print("UNSAT")
