from dataclasses import dataclass
import typing as tp


@dataclass
class CDCLTask:
    num_clauses: int
    num_variables: int
    clauses: tp.List[tp.List[int]]


@dataclass
class CDCLTaskDIMACS(CDCLTask):
    comment: str
    num_clauses: int
    num_variables: int
    clauses: tp.List[tp.List[int]]


@dataclass
class Decision:
    variable_number: int
    variable_value: int
    decision_level: int
    antecedent: int
